#!/usr/bin/env fish

set TIMELIMIT 1000
set g01 2 5
set g02 4 10
set g03 10 25
set g04 14 35
set g05 20 50
set g06 40 100
set g07 60 150
set g08 80 200
set g09 200 500
set g10 400 1000
set KS g01 g02 g03 g04 g05 g06 g07 g08 g09 g10

set INPUT ./project/instances/*.dat

for i in (seq (count $INPUT))
    for k in $$KS[$i]
        printf '%s' (set --show $KS[$i] | sed -n 's/.*\$\(.*\):.*/\1/p' | head -n 1) >> results.txt
        printf " & $k & " >> results.txt
        for s in $SOLVERS
            if test \( $s = mcf -o $s = mcfun \) -a \( $i -eq 9 -o $i -eq 10 \)
                printf "OOM" >> results.txt
            else
                set CMD ./run.sh -s $s -k $k -t $TIMELIMIT $INPUT[$i]
                $CMD 2>> results.txt
            end
            if [ $s != $SOLVERS[-1] ]
                printf ' & ' >> results.txt
            end
        end
        printf '%s%s\n' '\\' '\\' >> results.txt
    end
    if [ $i != (count $INPUT) ]
        printf '\\hline\n' >> results.txt
    end
end