#!/usr/bin/env bash

# From https://stackoverflow.com/a/246128/16084771
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

julia --project="$SCRIPT_DIR"/project "$SCRIPT_DIR"/project/src/main.jl "$@"