# Execution

Make sure to have Gurobi and Julia installed. Use the provided ```run.sh``` script to execute the solver. ```run.sh --help``` presents all possible options. For example, to solve the instance in file ```example.dat```, k=3 and using the multi-commodity flow formulation, run the script as follows:

```./run.sh -k 3 -s mcf ./example.dat```

Furthermore, ```benchmark-t1.fish``` and ```benchmark-t2.fish``` are provided for running the entire benchmark suite as stated
on the slides. The first script runs all compact formulations defined for the first programming exercise while the second one runs
all instances using all branch-and-cut formulations.