module constructions
using JuMP, Gurobi, Profile, Graphs, GraphsFlows, SimpleWeightedGraphs, Random, DataStructures

export construct_seq!, construct_scf!, construct_scf_undirected!, construct_mcf!, construct_mcf_undirected!, construct_frac_cec!, construct_int_cec!, construct_unsafe_dcc!, construct_safe_dcc!

function get_weight_override(graph, i, j)
    w = get_weight(graph, i, j)
    if w == 0.5
        0
    else
        w
    end
end

function construct_seq!(model, g, n::Int, k::Int)
    println("Using the sequential formulation")

    @variable(model, 1 <= u[i=1:n])
    @variable(model, x[i=1:n, j=2:n; has_edge(g, i, j)], Bin)

    @constraint(model, sum(x[1, j] for j in 1:n if has_edge(g, 1, j)) == 1)
    @constraint(model, [j = 2:n], sum(x[j, i] for i in 2:n if has_edge(g, i, j)) <= k * sum(x[i, j] for i in 1:n if has_edge(g, i, j)))
    @constraint(model, [i = 2:n], sum(x[j, i] for j in 1:n if has_edge(g, i, j)) <= 1)
    @constraint(model, sum(sum(x[i, j] for i in 2:n if has_edge(g, i, j)) for j in 2:n) == k - 1)
    @constraint(model, [i = 2:n, j = 2:n; has_edge(g, i, j)], u[i] + x[i, j] <= u[j] + (k - 1) * (1 - x[i, j]))
    @constraint(model, [i = 1:n], u[i] <= k)

    @objective(model, Min, sum(sum(x[i, j] * get_weight_override(g, i, j) for j in 2:n if has_edge(g, i, j)) for i in 1:n))
end

function construct_scf!(model, g, n::Int, k::Int)
    println("Using the directed SCF formulation")

    @variable(model, x[i=1:n, j=1:n; has_edge(g, i, j)], Bin)
    @variable(model, v[i=1:n], Bin)
    @variable(model, 0 <= f[i=1:n, j=2:n; has_edge(g, i, j)])

    @constraint(model, sum(f[1, j] for j in 2:n) == k)
    @constraint(model, sum(x[1, i] for i in 2:n) == 1)
    @constraint(model, [j = 2:n], sum(f[i, j] for i in 1:n if has_edge(g, i, j)) - sum(f[j, i] for i in 2:n if has_edge(g, i, j)) == v[j])
    @constraint(model, [i = 1:n, j = 2:n; has_edge(g, i, j)], f[i, j] <= k * x[i, j])
    @constraint(model, [i = 1:n, j = (i+1):n; has_edge(g, i, j)], x[i, j] + x[j, i] <= 1)
    @constraint(model, [i = 1:n, j = 2:n; has_edge(g, i, j)], x[i, j] <= v[j])

    @objective(model, Min, sum(sum(x[i, j] * get_weight_override(g, i, j) for j in 1:n if has_edge(g, i, j)) for i in 1:n))
end

function construct_scf_undirected!(model, g, n::Int, k::Int)
    println("Using the undirected SCF formulation")

    @variable(model, x[i=1:n, j=(i+1):n; has_edge(g, i, j)], Bin)
    @variable(model, v[i=1:n], Bin)
    @variable(model, 0 <= f[i=1:n, j=2:n; has_edge(g, i, j)])

    @constraint(model, sum(f[1, j] for j in 2:n) == k)
    @constraint(model, sum(x[1, i] for i in 2:n) == 1)
    @constraint(model, [j = 2:n], sum(f[i, j] for i in 1:n if has_edge(g, i, j)) - sum(f[j, i] for i in 2:n if has_edge(g, i, j)) == v[j])
    @constraint(model, [i = 1:n, j = 2:n; has_edge(g, i, j)], f[i, j] <= k * if i < j
        x[i, j]
    else
        x[j, i]
    end)
    @constraint(model, [i = 1:n, j = (i+1):n; has_edge(g, i, j)], 2 * x[i, j] <= v[i] + v[j])

    @objective(model, Min, sum(sum(x[i, j] * get_weight_override(g, i, j) for j in (i+1):n if has_edge(g, i, j)) for i in 1:n))
end

function construct_mcf!(model, g, n::Int, k::Int)
    println("Using the directed MCF formulation")

    @variable(model, x[i=1:n, j=1:n; has_edge(g, i, j)], Bin)
    @variable(model, v[i=1:n], Bin)
    @variable(model, 0 <= f[i=1:n, j=1:n, k=2:n; has_edge(g, i, j)])

    @constraint(model, sum(sum(f[1, j, k] for k in 2:n) for j in 2:n) == k)
    @constraint(model, [k = 2:n], sum(f[i, k, k] for i in 1:n if has_edge(g, i, k)) == v[k])
    @constraint(model, sum(x[1, j] for j in 2:n) == 1)
    @constraint(model, [j = 2:n, k = 2:n; j != k], sum(f[i, j, k] for i in 1:n if has_edge(g, i, j)) - sum(f[j, i, k] for i in 2:n if has_edge(g, j, i)) == 0)
    @constraint(model, [i = 1:n, j = 1:n, c = 2:n; has_edge(g, i, j)], f[i, j, c] <= x[i, j])
    @constraint(model, [i = 1:n, j = (i+1):n; has_edge(g, i, j)], x[i, j] + x[j, i] <= 1)
    @constraint(model, [i = 1:n, j = 2:n; has_edge(g, i, j)], x[i, j] <= v[j])

    @objective(model, Min, sum(sum(x[i, j] * get_weight_override(g, i, j) for j in 1:n if has_edge(g, i, j)) for i in 1:n))
end

function construct_mcf_undirected!(model, g, n::Int, k::Int)
    println("Using the undirected MCF formulation")

    @variable(model, x[i=1:n, j=(i+1):n; has_edge(g, i, j)], Bin)
    @variable(model, v[i=1:n], Bin)
    @variable(model, 0 <= f[i=1:n, j=1:n, k=2:n; has_edge(g, i, j)])

    @constraint(model, sum(sum(f[1, j, k] for k in 2:n) for j in 2:n) == k)
    @constraint(model, [k = 2:n], sum(f[i, k, k] for i in 1:n if has_edge(g, i, k)) == v[k])
    @constraint(model, sum(x[1, j] for j in 2:n) == 1)
    @constraint(model, [j = 2:n, k = 2:n; j != k], sum(f[i, j, k] for i in 1:n if has_edge(g, i, j)) - sum(f[j, i, k] for i in 2:n if has_edge(g, j, i)) == 0)
    @constraint(model, [i = 1:n, j = 1:n, c = 2:n; has_edge(g, i, j)], f[i, j, c] <= if i < j
        x[i, j]
    else
        x[j, i]
    end)
    @constraint(model, [i = 1:n, j = (i+1):n; has_edge(g, i, j)], 2 * x[i, j] <= v[i] + v[j])

    @objective(model, Min, sum(sum(x[i, j] * get_weight_override(g, i, j) for j in (i+1):n if has_edge(g, i, j)) for i in 1:n))
end

function construct_frac_cec!(model, g, n::Int, k::Int)
    construct_cec!(model, g, k, true)
end

function construct_int_cec!(model, g, n::Int, k::Int)
    construct_cec!(model, g, k, false)
end

function construct_cec!(model, g, k::Int, frac::Bool)
    println("Using the CEC formulation")
    un_graph = SimpleGraph(g)
    di_graph = SimpleDiGraph(un_graph)

    @variable(model, x[i=edges(di_graph)], Bin)
    @variable(model, y[i=vertices(di_graph)], Bin)

    @constraint(model, sum(x[e] for e in edges(di_graph) if src(e) != 1 && dst(e) != 1) == k - 1)
    @constraint(model, [v = filter(v -> v != 1, vertices(di_graph))], sum(x[Edge(v, n)] for n in outneighbors(di_graph, v)) <= k * y[v])
    @constraint(model, [v = filter(v -> v != 1, vertices(di_graph))], sum(x[Edge(n, v)] for n in inneighbors(di_graph, v)) == y[v])
    @constraint(model, sum(x[Edge(1, n)] for n in outneighbors(di_graph, 1)) == 1)
    @constraint(model, sum(x[Edge(n, 1)] for n in inneighbors(di_graph, 1)) == 0)
    @constraint(model, [e = edges(un_graph)], x[Edge(src(e), dst(e))] + x[Edge(dst(e), src(e))] <= y[dst(e)])

    @objective(model, Min, sum(x[e] * get_weight_override(g, src(e), dst(e)) for e in edges(di_graph)))

    numLazy = 0

    # If we also consider fractional solutions, use a larger threshold
    # This means that an edge x_{i, j} counts as selected if x_{i, j} > 1 - threshold
    threshold = if frac
        0.75
    else
        MOI.get(model, MOI.RawOptimizerAttribute("MIPGap"))
    end

    function cec_trampoline(cb_data, cb_where::Cint)
        if cb_where == GRB_CB_MIPSOL || (frac && cb_where == GRB_CB_MIPNODE)
            if cb_where == GRB_CB_MIPNODE
                resultP = Ref{Cint}()
                GRBcbget(cb_data, cb_where, GRB_CB_MIPNODE_STATUS, resultP)
                if resultP[] != GRB_OPTIMAL
                    # Not optimal
                    return
                end
            end
            Gurobi.load_callback_variable_primal(cb_data, cb_where)
            numLazy += cec_callback(di_graph, model, cb_data, x, threshold)
        end
    end

    MOI.set(model, MOI.RawOptimizerAttribute("LazyConstraints"), 1)
    MOI.set(model, Gurobi.CallbackFunction(), cec_trampoline)

    return () -> numLazy
end

function cec_callback(graph, model, cb_data, x, threshold)
    # Important to create all vertices in order to reference correct edge
    tmp_g = SimpleDiGraph(nv(graph))
    for e in Iterators.filter(e -> callback_value(cb_data, x[e]) > 1 - threshold, edges(graph))
        add_edge!(tmp_g, e)
    end

    numConstr = 0
    for c in simplecycles(tmp_g)
        con = @build_constraint(
            sum(x[Edge(e[1], e[2])] for e in ((c[i:i+1] for i in 1:length(c)-1)))
            +
            x[Edge(last(c), c[1])] <= length(c) - 1)
        MOI.submit(model, MOI.LazyConstraint(cb_data), con)
        numConstr += 1
    end

    return numConstr
end

struct DccConfig
    graph::SimpleDiGraph{Int}
    nr_graph::SimpleDiGraph{Int}
    weights::AbstractMatrix{Int}
    distances::AbstractMatrix{Int}
    shortest_paths::Matrix{Union{Vector{Graphs.SimpleGraphs.SimpleEdge{Int}},Nothing}}
    threshold::Float64
    DccConfig(g::SimpleDiGraph{Int}, w::AbstractMatrix{Int}, t::Float64) = begin
        nr_graph = SimpleDiGraph(g)
        for e in Iterators.filter(e -> src(e) == 1 || dst(e) == 1, edges(g))
            rem_edge!(nr_graph, e)
        end

        distances = zeros(Int, nv(g), nv(g))
        for v in vertices(g)
            dist = dijkstra_shortest_paths(nr_graph, v, w)
            for (target, d) in enumerate(dist.dists)
                distances[v, target] = if d == Inf
                    0
                else
                    d
                end
            end
        end

        shortest_paths::Matrix{Union{Vector{Graphs.SimpleGraphs.SimpleEdge{Int}},Nothing}} = fill(nothing, nv(g), nv(g))

        new(g, nr_graph, w, distances, shortest_paths, t)
    end
end

function construct_safe_dcc!(model, g, n::Int, k::Int)
    construct_dcc!(model, g, k, dcc_safe_callback, true)
end

function construct_unsafe_dcc!(model, g, n::Int, k::Int)
    construct_dcc!(model, g, k, dcc_unsafe_callback, false)
end

function construct_dcc!(model, g, k::Int, callback, frac::Bool)
    println("Using the DCC formulation")
    un_graph = SimpleGraph(g)
    di_graph = SimpleDiGraph(un_graph)

    w = collect(map(w -> if w == 0.5
            0
        else
            round(Int, w)
        end, weights(g)))
    @time "Preparation time" config = DccConfig(di_graph, w, MOI.get(model, MOI.RawOptimizerAttribute("MIPGap")))

    @variable(model, x[i=edges(di_graph)], Bin)
    @variable(model, y[i=vertices(di_graph)], Bin)

    @constraint(model, sum(x[e] for e in edges(di_graph) if src(e) != 1 && dst(e) != 1) == k - 1)
    @constraint(model, [v = filter(v -> v != 1, vertices(di_graph))], sum(x[Edge(v, n)] for n in outneighbors(di_graph, v)) <= k * y[v])
    @constraint(model, [i = vertices(di_graph)], sum(x[Edge(j, i)] for j in inneighbors(di_graph, i)) == y[i])
    @constraint(model, sum(x[Edge(1, n)] for n in outneighbors(di_graph, 1)) == 1)
    @constraint(model, sum(x[Edge(n, 1)] for n in inneighbors(di_graph, 1)) == 0)
    @constraint(model, [e = edges(un_graph)], x[Edge(src(e), dst(e))] + x[Edge(dst(e), src(e))] <= y[dst(e)])

    @objective(model, Min, sum(x[e] * get_weight_override(g, src(e), dst(e)) for e in edges(di_graph) if src(e) != 1 && dst(e) != 1))

    numLazy = 0

    function dcc_trampoline(cb_data, cb_where::Cint)
        if cb_where == GRB_CB_MIPSOL || (frac && cb_where == GRB_CB_MIPNODE)
            if cb_where == GRB_CB_MIPNODE
                resultP = Ref{Cint}()
                GRBcbget(cb_data, cb_where, GRB_CB_MIPNODE_STATUS, resultP)
                if resultP[] != GRB_OPTIMAL
                    # Not optimal
                    return
                end
            end
            Gurobi.load_callback_variable_primal(cb_data, cb_where)
            numLazy += callback(cb_data, config, model, x, y)
        end
    end

    MOI.set(model, MOI.RawOptimizerAttribute("LazyConstraints"), 1)
    MOI.set(model, Gurobi.CallbackFunction(), dcc_trampoline)

    return () -> numLazy
end

# Constructs a directed cutset constraint 
# part1 and part2 present the partition of the cut.
# actual represents the vertices to which the root node should be connected to as well.
function construct_dcc_constraint(graph, part1, actual, part2, x, y)
    cut_vars = []
    for from in filter(v -> v != 1, part1), to in part2
        e = Edge(from, to)
        if e in edges(graph)
            push!(cut_vars, x[e])
        end
    end
    for to in actual
        push!(cut_vars, x[Edge(1, to)])
    end
    @build_constraint(sum(v for v in cut_vars) >= y)
end

# BFS to remove vertices that are connected to the root
# Returns all removed vertices
function remove_connected_vertices!(vertices, config, weight_matrix)
    queue = Deque{Int}()
    push!(queue, 1)
    visited = Set{Int}()
    while !isempty(queue)
        current = popfirst!(queue)
        push!(visited, current)
        for n in filter(v -> abs(weight_matrix[current, v] - 1) <= config.threshold && v in vertices, outneighbors(config.graph, current))
            push!(queue, n)
            delete!(vertices, n)
        end
    end

    visited
end

# Creates weight matrix. An edge has a weight between [0,1]
# Returns the weight matrix and the set of selected vertices
function dcc_construct_graph(config::DccConfig, cb_data, x, useInt::Bool)
    t = useInt ? Int : Float64
    weight_matrix = zeros(t, nv(config.graph), nv(config.graph))
    # Use set to ensure no duplicates are stored
    selected_vertices::Set{Int} = Set()

    for e in edges(config.graph)
        val = callback_value(cb_data, x[e])
        weight_matrix[src(e), dst(e)] = useInt ? round(Int, val) : val
        if abs(weight_matrix[src(e), dst(e)] - 1) <= config.threshold
            push!(selected_vertices, dst(e))
        end
    end

    result = remove_connected_vertices!(selected_vertices, config, weight_matrix)

    # Convert set to array and randomize
    shuffled_vertices = shuffle!(collect(selected_vertices))

    (weight_matrix, shuffled_vertices, result, setdiff(vertices(config.graph), result))
end

function dcc_unsafe_callback(cb_data, config, model, x, y)
    (_, selected_vertices, part1, part2) = dcc_construct_graph(config, cb_data, x, true)

    if !isempty(selected_vertices)
        target = first(selected_vertices)
        con = construct_dcc_constraint(config.graph, part1, part2, selected_vertices, x, y[target])
        MOI.submit(model, MOI.LazyConstraint(cb_data), con)
        # Added one constraint
        1
    else
        0
    end
end

function dcc_safe_callback(cb_data, config, model, x, y)
    (weight_matrix, selected_vertices, _, _) = dcc_construct_graph(config, cb_data, x, false)

    num_added_constr = 0
    for target in selected_vertices
        (part1, part2, flow) = GraphsFlows.mincut(config.graph, 1, target, weight_matrix, EdmondsKarpAlgorithm())
        if flow < 1
            con = construct_dcc_constraint(config.graph, part1, part2, part2, x, y[target])
            MOI.submit(model, MOI.LazyConstraint(cb_data), con)
            num_added_constr += 1
            break
        end
    end

    return num_added_constr
end
end
