include("constructions.jl")

using JuMP, Gurobi, Graphs, GraphsFlows, SimpleWeightedGraphs, ArgParse, Printf, .constructions

@enum Solver seq scf scfun mcf mcfun fraccec cec dcc unsafedcc

# From https://discourse.julialang.org/t/retrieving-an-instance-of-an-enum-using-a-string/61183/6
Base.tryparse(E::Type{<:Enum}, str::String) =
let insts = instances(E),
    p = findfirst(==(Symbol(str)) ∘ Symbol, insts)
    
    p !== nothing ? insts[p] : nothing
end

function get_constructor(formulation)
    formulation == seq && return construct_seq!
    formulation == scf && return construct_scf!
    formulation == scfun && return construct_scf_undirected!
    formulation == mcf && return construct_mcf!
    formulation == mcfun && return construct_mcf_undirected!
    formulation == fraccec && return construct_frac_cec!
    formulation == cec && return construct_int_cec!
    formulation == dcc && return construct_safe_dcc!
    formulation == unsafedcc && return construct_unsafe_dcc!
end

function parse_commandline()
    s = ArgParseSettings()
    
    @add_arg_table s begin
        "--solver", "-s"
        help = "Formulation to use, allowed values: " * string(map(i -> String(Symbol(i)), instances(Solver)))
        required = true
        "--draw"
        help = "Output solution as a PDF in the current directory. Requires fontconfig and cairo."
        action = :store_true
        "-k"
        help = "Size of the MST. Must be smaller or equal to the number of nodes"
        arg_type = Int
        required = true
        "-t"
        help = "Set a time limit in seconds for the MILP solver"
        arg_type = Int
        default = 100
        "instance"
        help = "Path to instance to solve"
        required = true
    end
    
    return parse_args(s)
end

args = parse_commandline()

mutable struct Input
    num_nodes::Int
    num_edges::Int
    edges::Vector{Tuple{Int,Int,Int}}
    Input() = new(0, 0, [])
end

config_graph = Input()
open(args["instance"], "r") do io
    for (i, line) in enumerate(readlines(io))
        if i == 1
            config_graph.num_nodes = parse(Int, line)
        elseif i == 2
            config_graph.num_edges = parse(Int, line)
        elseif line == ""
            continue
        else
            columns = split(line, " ")
            edge = Tuple(parse(Int, x) for x in view(columns, 2:4))
            push!(config_graph.edges, edge)
        end
    end
end

g = SimpleWeightedGraph(config_graph.num_nodes)

for (from, to, weight) in config_graph.edges
    weight = if weight == 0
        0.5
    else
        weight
    end
    
    if !add_edge!(g, from + 1, to + 1, weight)
        println("Could not add edge")
        return
    end
end

formulation = tryparse(Solver, args["solver"])

model = direct_model(Gurobi.Optimizer())
set_optimizer_attribute(model, "TimeLimit", args["t"])

construct = get_constructor(formulation)

println("Constructing model")
numLazy = construct(model, g, config_graph.num_nodes, args["k"])

println("Solving model")
optimize!(model)
print("Solver stopped for instance $(args["instance"]) (k=$(args["k"]), formulation=$(args["solver"])): ")
if has_values(model)
    duration = if termination_status(model) == TIME_LIMIT
        "TL"
    else
        @sprintf "%.2f s" solve_time(model)
    end
    rel_gap = @sprintf "%.2f" relative_gap(model) * 100
    
    lazy_constraints = if formulation == cec || formulation == dcc || formulation == fraccec || formulation == unsafedcc
        "$(numLazy()), "
    else
        ""
    end
    
    print(stderr, "$(round(Int, objective_value(model))) ($(node_count(model)), $(lazy_constraints)$(rel_gap) \\%, $(duration))")
else
    println("No optimal solution found, reason: $(termination_status(model))")
    print(stderr, "ERROR")
end

# Verify
function extract(src, dst)
    v = variable_by_name(model, "x[$src,$dst]")
    if v !== Nothing
        value(v)
    else
        0.0
    end
end

function extract2(src, dst)
    v = variable_by_name(model, "x[$(Edge(src, dst))]")
    if v !== Nothing
        value(v)
    else
        0.0
    end
end

function verify(g, extractor)
    c = SimpleDiGraph(SimpleGraph(g))
    for e in edges(g)
        if extractor(src(e), dst(e)) < 0.9
            rem_edge!(c, src(e), dst(e))
        end
        if extractor(dst(e), src(e)) < 0.9
            rem_edge!(c, dst(e), src(e))
        end
    end
    
    toBeRemoved = collect(filter(v -> size(all_neighbors(c, v), 1) == 0, vertices(c)))
    rem_vertices!(c, toBeRemoved)
    
    println("Is tree: $(is_tree(SimpleGraph(c)))")
    println("|V|: $(nv(c))")
    println("|E|: $(ne(c))")
    
    for c in cycle_basis(c)
        println("CYCLE:", c)
    end
    
    c
end

# c = verify(g, extract2)

if args["draw"]
    using GraphPlot, Compose
    import Cairo, Fontconfig
    
    nodelabel = (1:nv(g))
    edgelabel = map(e -> 
    if e.weight == 0.5
        0
    else
        e.weight
    end, edges(g))

    edgecolor = map(e ->
    begin
        arc1 = extract2(src(e), dst(e)) >= 0.9
        arc2 = extract2(dst(e), src(e)) >= 0.9
        if arc1 && arc2
            "red"
        elseif arc1 || arc2
            "blue"
        else
            "lightgray"
        end
    end, edges(g))
    
    draw(PDF("output.pdf", 32cm, 32cm), gplot(g, nodelabel=nodelabel, edgelabel=edgelabel, edgestrokec=edgecolor))
    # draw(PDF("output2.pdf", 64cm, 64cm), gplot(c, arrowlengthfrac=0))
end