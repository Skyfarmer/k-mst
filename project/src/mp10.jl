using JuMP, Gurobi

model = direct_model(Gurobi.Optimizer())

@variable(model, 0 <= x)
@variable(model, 0 <= u)

@constraint(model, x >= u * 4)
@constraint(model, x >= 14)
@constraint(model, x >= 4 + u * 3)
@constraint(model, x >= 10 + u)
@constraint(model, x >= 4 + 14 + u * -1)
@constraint(model, x >= 10 + 14 + u * -3)
@constraint(model, x >= 10 + 4)
@constraint(model, x >= 10 + 4 + 14 + u * -4)

@objective(model, Min, x)

optimize!(model)
println(model)
println("Optimal value: $(objective_value(model))")
println("u: $(value(u))")