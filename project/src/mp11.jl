using JuMP, Gurobi
 
model = direct_model(Gurobi.Optimizer())

n = 4
W = 10
weights = [6, 6, 6, 6]

@variable(model, 0 <= y[i=1:n] <= 1)
@variable(model, 0 <= x[i=1:n, j=1:n] <= 1)

@constraint(model, [i = 1:n], sum(weights[j] * x[i, j] for j = 1:n) <= W * y[i])
@constraint(model, [j = 1:n], sum(x[i, j] for i = 1:n) == 1)

@objective(model, Min, sum(y))

optimize!(model)
println(model)
println("Optimal value: $(objective_value(model))")
println("y: $(value.(y))")
println("x: $(value.(x))")