using JuMP, Gurobi

model = Model(Gurobi.Optimizer)
set_optimizer_attribute(model, "TimeLimit", 100)
set_optimizer_attribute(model, "Presolve", 0)

n = 18
k = 3

@variable(model, x[i = 1:n] >= 0)
@variable(model, dhome[i = 1:n, i, j = 1:n; i != j], Bin)
@variable(model, dfar[i = 1:n, j = 1:n, i; i != j], Bin)
@variable(model, whome[i = 1:n, i, j = 1:n; i != j], Bin)
@variable(model, wfar[i = 1:n, j = 1:n, i; i != j], Bin)

@constraint(model, [i = 1:n, j = 1:n; i != j], whome[i, i, j] + wfar[i, j, i] <= 1)
@constraint(model, [i = 1:n, j = 1:n; i != j], dhome[i, i, j] + dfar[i, j, i] == 2 * (1 - whome[i, i, j] - wfar[i, j, i]))
@constraint(model, [i = 1:n-1], x[i] <= x[i + 1])
@constraint(model, [i = 1:n], x[i] == sum(3 * whome[i, i, j] + 3 * wfar[j, i, j] + dhome[i, i, j] + dfar[j, i, j] for j in 1:n if i != j))

@objective(model, Max, x[k])

optimize!(model)

println(value(x[k]))